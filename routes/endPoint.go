package routes

import (
        "latihan_go/service"

        "github.com/labstack/echo"
)

//RoleEndPoint function
func Endpoint() {
        e := echo.New()
        //roles endpoint
        e.GET("/role/readAll", service.ReadAllRole)
        e.POST("/role/save", service.CreateRole)
        e.DELETE("/role/delete", service.DeleteRole)
        e.PATCH("/role/update", service.UpdateRole)
        e.GET("/role/read", service.ReadIdRole)

        e.Logger.Fatal(e.Start(":1323"))
}
